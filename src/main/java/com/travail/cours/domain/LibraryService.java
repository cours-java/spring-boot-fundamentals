package com.travail.cours.domain;

import com.travail.cours.infra.LibraryEntity;
import com.travail.cours.infra.LibraryEntityDao;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LibraryService {
    private final LibraryEntityDao repository;

    public LibraryService(LibraryEntityDao repository) {
        this.repository = repository;
    }

    public boolean isBookAlreadyExists(String bookId){
        Optional<LibraryEntity> entityOptional = repository.findById(bookId);
        return entityOptional.isPresent();
    }
}
