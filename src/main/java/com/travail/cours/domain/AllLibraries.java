package com.travail.cours.domain;

import java.util.List;
import java.util.Optional;

public interface AllLibraries {
    void stock(Library library);
    Optional<Library> getBook(String id);
    List<Library> booksByAuthor(String authorName);
}
