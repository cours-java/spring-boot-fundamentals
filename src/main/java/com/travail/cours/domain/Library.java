package com.travail.cours.domain;

import java.util.Objects;

public class Library {
    private String name;
    private String id;
    private String isbn;
    private int aisle;
    private String author;

    public Library() {
    }

    public Library(String name, String id, String isbn, int aisle, String author) {
        this.name = name;
        this.id = id;
        this.isbn = isbn;
        this.aisle = aisle;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getAisle() {
        return aisle;
    }

    public void setAisle(int aisle) {
        this.aisle = aisle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Library library = (Library) o;
        return aisle == library.aisle && Objects.equals(name, library.name) && Objects.equals(id, library.id) && Objects.equals(isbn, library.isbn) && Objects.equals(author, library.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, isbn, aisle, author);
    }
}
