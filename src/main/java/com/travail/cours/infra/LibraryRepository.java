package com.travail.cours.infra;

import com.travail.cours.domain.AllLibraries;
import com.travail.cours.domain.Library;
import com.travail.cours.web.Mapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class LibraryRepository implements AllLibraries {
    public LibraryEntityDao dao;
    public Mapper mapper;

    public LibraryRepository(LibraryEntityDao dao, Mapper mapper) {
        this.dao = dao;
        this.mapper = mapper;
    }

    @Override
    public void stock(Library library) {
        LibraryEntity entity = mapper.libraryToEntity(library);
        dao.save(entity);
    }

    @Override
    public Optional<Library> getBook(String id) {
        Optional<LibraryEntity> entityOptional = dao.findById(id);
        return mapper.entityToLibrary(entityOptional.orElse(null));
    }

    @Override
    public List<Library> booksByAuthor(String authorName) {
        List<LibraryEntity> entities = dao.findAllByAuthor(authorName);
        List<Library> libraries = new ArrayList<>();

        for (LibraryEntity entity : entities) {
            mapper.entityToLibrary(entity).ifPresent(libraries::add);
        }

        return libraries;
    }
}
