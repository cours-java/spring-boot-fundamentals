package com.travail.cours.infra;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name="library")
public class LibraryEntity {

    @Column(name = "book_name")
    private String name;

    @Column(name = "id")
    @Id
    private String id;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "aisle")
    private int aisle;

    @Column(name = "author")
    private String author;

    public LibraryEntity() {
    }

    public LibraryEntity(String name, String id, String isbn, int aisle, String author) {
        this.name = name;
        this.id = id;
        this.isbn = isbn;
        this.aisle = aisle;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String bookName) {
        this.name = bookName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getAisle() {
        return aisle;
    }

    public void setAisle(int aisle) {
        this.aisle = aisle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibraryEntity that = (LibraryEntity) o;
        return aisle == that.aisle && Objects.equals(name, that.name) && Objects.equals(id, that.id) && Objects.equals(isbn, that.isbn) && Objects.equals(author, that.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, isbn, aisle, author);
    }
}
