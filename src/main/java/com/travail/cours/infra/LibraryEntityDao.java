package com.travail.cours.infra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LibraryEntityDao extends JpaRepository<LibraryEntity, String> {
    Optional<LibraryEntity> findById(String id);
    List<LibraryEntity> findAllByAuthor(String authorName);
}
