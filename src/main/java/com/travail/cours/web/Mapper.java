package com.travail.cours.web;

import com.travail.cours.domain.Library;
import com.travail.cours.infra.LibraryEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class Mapper {

    public Optional<Library> entityToLibrary(LibraryEntity entity) {

        if (entity == null) {
            return Optional.empty();
        }

        Library library = new Library();

        library.setAisle(entity.getAisle());
        library.setAuthor(entity.getAuthor());
        library.setName(entity.getName());
        library.setId(entity.getId());
        library.setIsbn(entity.getIsbn());

        return Optional.of(library);
    }

    public LibraryEntity libraryToEntity(Library library) {
        LibraryEntity entity = new LibraryEntity();

        entity.setAisle(library.getAisle());
        entity.setAuthor(library.getAuthor());
        entity.setName(library.getName());
        entity.setId(library.getId());
        entity.setIsbn(library.getIsbn());

        return entity;
    }

    public LibraryRessource libraryToResource(Library library) {
        LibraryRessource ressource = new LibraryRessource();

        ressource.setAisle(library.getAisle());
        ressource.setAuthor(library.getAuthor());
        ressource.setName(library.getName());
        ressource.setId(library.getId());
        ressource.setIsbn(library.getIsbn());

        return ressource;
    }

    public Library resourceToLibrary(LibraryRessource resource) {
        Library library = new Library();

        library.setAisle(resource.getAisle());
        library.setAuthor(resource.getAuthor());
        library.setName(resource.getName());
        library.setId(resource.getId());
        library.setIsbn(resource.getIsbn());

        return library;
    }
}
