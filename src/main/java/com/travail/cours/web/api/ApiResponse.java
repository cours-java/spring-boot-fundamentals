package com.travail.cours.web.api;

public class ApiResponse {
    private String message;
    private String bookId;

    public ApiResponse(String message, String bookId) {
        this.message = message;
        this.bookId = bookId;
    }

    public ApiResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }
}
