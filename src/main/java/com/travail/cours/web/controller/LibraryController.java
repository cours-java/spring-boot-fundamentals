package com.travail.cours.web.controller;

import com.travail.cours.domain.AllLibraries;
import com.travail.cours.domain.Library;
import com.travail.cours.domain.LibraryService;
import com.travail.cours.web.LibraryRessource;
import com.travail.cours.web.Mapper;
import com.travail.cours.web.api.ApiResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LibraryController {
    AllLibraries storage;
    LibraryService service;
    Mapper mapper;

    public LibraryController(AllLibraries storage, LibraryService service, Mapper mapper) {
        this.storage = storage;
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping("/newBook")
    public ResponseEntity<ApiResponse> addBook(@RequestBody LibraryRessource resource) {
        boolean exists = service.isBookAlreadyExists(resource.getId());

        Library library = mapper.resourceToLibrary(resource);

        ApiResponse response = new ApiResponse();
        response.setBookId(resource.getId());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Unique", resource.getId());

        if (exists) {
            response.setMessage("Books already exists");
            return new ResponseEntity<>(response, headers, HttpStatus.ACCEPTED);
        }

        storage.stock(library);

        response.setMessage("Success book is added");
        return new ResponseEntity<>(response, headers, HttpStatus.CREATED);
    }

    @GetMapping("/books/{id}")
    public LibraryRessource getBookById(@PathVariable(value = "id") String id) {
        Library library = storage.getBook(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return mapper.libraryToResource(library);
    }

    @GetMapping("/books/author")
    public List<LibraryRessource> getBookByAuthor(@RequestParam(value = "authorName") String authorName) {
        List<Library> libraries = storage.booksByAuthor(authorName);
        List<LibraryRessource> ressources = new ArrayList<>();

        for (Library library : libraries) {
            ressources.add(mapper.libraryToResource(library));
        }

        return ressources;
    }
}
