package com.travail.cours.web;

public class LibraryRessource {
    private String name;
    private String id;
    private String isbn;
    private int aisle;
    private String author;

    public LibraryRessource(String name, String id, String isbn, int aisle, String author) {
        this.name = name;
        this.id = id;
        this.isbn = isbn;
        this.aisle = aisle;
        this.author = author;
    }

    public LibraryRessource() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getAisle() {
        return aisle;
    }

    public void setAisle(int aisle) {
        this.aisle = aisle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
